'use strict';

/**
 * @ngdoc function
 * @name heroAppApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the heroAppApp
 */
angular.module('heroAppApp').controller('heroController', function ($scope,heroContentService,$state) {
   $scope.heroList= [];
   $scope.heroName = '';
   $scope.currentId = 10;
   $scope.addHero = function(){
   	if($scope.heroName === ''){return }
   	 var heroObj = {
      		'id':$scope.currentId + $scope.heroList.length,
      		'name':$scope.heroName
      	}
   	heroContentService.saveHero(heroObj);   	 
   	$scope.heroName = '';   	
   };
$scope.loadHeroDetails = function(){
	$scope.heroList = heroContentService.getAllHero();
}

   $scope.removeHero = function(heroId){
   		var selectedHero_index = $scope.heroList.indexOf(heroId);
   		if(selectedHero_index !== -1){
   			$scope.heroList.splice(selectedHero_index,1)
   		}
	};
	$scope.viewDetail = function(){
			$state.go('heroindetail');
		}
	$scope.selectedHero = null;

	$scope.selectAHero = function(selectedHeroObj){
		$scope.selectedHero = selectedHeroObj;
		heroContentService.setHeroDetails(selectedHeroObj);
	}

	$scope.loadHeroDetails();// to load the initial hero list;
	
  });
