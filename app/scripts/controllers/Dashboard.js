'use strict';

/**
 * @ngdoc function
 * @name heroAppApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the heroAppApp
 */
angular.module('heroAppApp')
  .controller('DashboardCtrl', function ($scope,heroContentService,$state) {
  

    $scope.loadHeroDetails = function(){
		$scope.topHeros = heroContentService.getAllHero();
	}();
	
	$scope.viewDetails = function(hero){
		heroContentService.setHeroDetails(hero);
			$state.go('heroindetail');
		}

  });
