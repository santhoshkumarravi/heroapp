'use strict';

/**
 * @ngdoc function
 * @name heroAppApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the heroAppApp
 */
angular.module('heroAppApp')
  .controller('HeroInDetailCtrl', function ($scope,heroContentService,$location) {  
		$scope.heroDetail = heroContentService.getHeroDetails();	

		$scope.save = function(hero){
			heroContentService.saveHero(hero);
			$location.url('/heros');
		}
		$scope.moveBack = function(){
			$location.url('/heros');
		}
  });
