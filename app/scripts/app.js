'use strict';

/**
 * @ngdoc overview
 * @name heroAppApp
 * @description
 * # heroAppApp
 *
 * Main module of the application.
 */
angular
  .module('heroAppApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
       'ngSanitize',
    'ngTouch',
    'ui.router'
  ])
  .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise('default');
    $stateProvider.state('default',{
      url:"/",
      templateUrl:'views/dashboard.html',
      controller: 'DashboardCtrl'
    }).state('heros',{
      url:'/heros',
      templateUrl: 'views/hero.html',
      controller: 'heroController'
    }).state('heroindetail',{
      url:'heroindetail',
      templateUrl:'views/heroindetail.html',
      controller: 'HeroInDetailCtrl'
    });
  }]);
/*  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/dashboard.html',
        controller: 'DashboardCtrl'
      })
      .when('/heros', {
        templateUrl: 'views/hero.html',
        controller: 'heroController'
      })
      .when('/heroindetail', {
        templateUrl: 'views/heroindetail.html',
        controller: 'HeroInDetailCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });*/
