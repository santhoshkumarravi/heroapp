'use strict';

/**
 * @ngdoc function
 * @name heroAppApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the heroAppApp
 */
angular.module('heroAppApp').service('heroContentService', function () {
 this.selectedHeroDetail ;
  this.heroList = [{
  	'id':10,
  	'name':'santhosh'
  },
  {
    'id':11,
    'name':'santhosh'
  },{
    'id':12,
    'name':'santhosh'
  }];

   this.saveHero = function(hero){
   	if(!hero){return}
   		var foundHero = this.heroList.indexOf(hero);
   		if(foundHero !== -1){
			this.deleteHero(hero);
   		}
   		this.heroList.push(hero);
   		
   		
   }
   this.deleteHero = function(hero){
	   	var selectedHero_index = this.heroList.indexOf(hero);
	   		if(selectedHero_index !== -1){
	   			this.heroList.splice(selectedHero_index,1)
	   		}
   }

   this.getAllHero = function(){
   	return this.heroList;
   }
	this.getHeroDetails = function(){
		return this.selectedHeroDetail;
	}
	this.setHeroDetails = function(selectedHero){
		return this.selectedHeroDetail = selectedHero;
	}
});